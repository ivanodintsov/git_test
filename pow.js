const pow = pow => num => parseInt(num) ** parseInt(pow);

export default pow;
