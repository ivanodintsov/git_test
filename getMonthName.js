const months = [
  'january',
  'february',
  'march',
  'april',
  'may',
  'june',
  'july',
  'august',
  'september',
  'october',
  'november',
  'december'
];

const getMonthName = place => {
  const monthIndex = place - 1;

  if (months[monthIndex] === undefined) throw 'The month number is incorrect.';
  return months[monthIndex];
};
