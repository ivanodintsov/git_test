const volume = x => y => z => parseInt(x) * parseInt(y) * parseInt(z);

export default volume;
